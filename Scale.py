import os
from gimpfu import *
import gimpfu
import logging

# Not tested

def scale(logo, tdrawable, imageName):
    """ https://stackoverflow.com/questions/44251764/gimp-python-fu-align-layer-with-respect-to-background-layer-in-image """
    logger = logging.getLogger()
    logger.info('got logo file {0}'.format(imageName))

    newImage = pdb.gimp_image_new(450, 300, RGB)

    newLayer = pdb.gimp_layer_new(newImage, 450, 300, 0, "background", 100.0, NORMAL_MODE)

    pdb.gimp_context_set_background((255, 255, 255))
    pdb.gimp_drawable_fill(newLayer, gimpfu.BACKGROUND_FILL)
    logger.info('Created new background image {0}')
    newImage.add_layer(newLayer, 0)

    logger.info('loading logo {0}'.format(imageName))
    logo = pdb.gimp_file_load(imageName, imageName)

    visibleLayer = pdb.gimp_layer_new_from_visible(logo, newImage, 'logo')
    pdb.gimp_image_add_layer(newImage, visibleLayer, 0)

    logger.info('Scaling logo to 435 width, 100 height')
    pdb.gimp_layer_scale(visibleLayer, 435, 100, True)

    logger.info('Align logo to background image')
    offx = (newLayer.width - visibleLayer.width) / 2
    offy = (newLayer.height - visibleLayer.height) / 2
    pdb.gimp_layer_set_offsets(visibleLayer, offx, offy)
    logger.info('Sharpening logo')
    pdb.plug_in_unsharp_mask(None, visibleLayer, 100.0, 0.5, 0)